function formatDate(dateString) {
    const options = { year: '2-digit', month: '2-digit', day: '2-digit' };
    return new Date(dateString).toLocaleDateString(undefined, options);


  }function createCard(name, description, pictureUrl, starts, ends, location) {
    const formattedStarts = formatDate(starts);
    const formattedEnds = formatDate(ends);

    return `
      <div class="card shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${formattedStarts} - ${formattedEnds}</div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Handle bad response
      } else {
        const data = await response.json();
        const container = document.querySelector('.card-container');
        let currentRow;
        for (let i = 0; i < data.conferences.length; i++) {
          const conference = data.conferences[i];
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, starts, ends, location);

            if (i % 3 === 0) {
              currentRow = document.createElement('div');
              currentRow.classList.add('row');
              container.appendChild(currentRow);
            }

            const column = document.createElement('div');
            column.classList.add('col', 'mb-4'); // Add mb-4 class for spacing
            column.innerHTML = html;
            currentRow.appendChild(column);
          }
        }
      }
    } catch (e) {
        const errorAlert = document.getElementById('error-alert');
        errorAlert.textContent = "An error occurred. Please try again later.";
        errorAlert.style.display = "block";
      }
  });
